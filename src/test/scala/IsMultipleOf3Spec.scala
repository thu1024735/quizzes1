import org.scalatest._
import Quizzes._

/**
  * Created by mark on 05/03/2017.
  */
class IsMultipleOf3Spec extends FunSuite{


  test("111 is multiple of 3"){
    assert(isMultipleOf3(111))
  }

  test("0 is multiple of 3"){
    assert(isMultipleOf3(0))
  }
  test("4 is not multiple of 3"){
    assert(!isMultipleOf3(4))
  }
}

object Quizzes {
  //判斷一個正整數是否為3的倍數？
  def isMultipleOf3(n:Int):Boolean= n%3==0

  //實現加總函數1+2+...+n
  def summation(n:Int):Int=n*(n+1)/2
}
