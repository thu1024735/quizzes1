/**
  * Created by mark on 05/03/2017.
  */
object Quizzes {
  //判斷一個正整數是否為3的倍數？
  def isMultipleOf3(n:Int):Boolean= n%3==0

  //實現加總函數1+2+...+n
  def summation(n:Int):Int=n*(n+1)/2
}
